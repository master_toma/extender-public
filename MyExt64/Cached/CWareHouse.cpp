
#include <Cached/CWareHouse.h>
#include <Cached/CUser.h>
#include <Cached/CItem.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/Config.h>

namespace Cached {

CWareHouse::BuySellExt CWareHouse::buySellExt[24];

void CWareHouse::Init()
{
	if (Config::Instance()->server->protocolVersion < Config::ProtocolVersionGraciaEpilogue) {
		return;
	}

	WriteMemoryBYTES(0x577582, "\x49\x89\xE0\x90\x90\x90\x90", 7); // mov r8, rsp
	WriteInstructionCall(0x577589, FnPtr(CWareHouse::AssembleSoldItem));

	WriteMemoryBYTES(0x5771C3, "\x48\x89\xE2\x90\x90\x90\x90", 7); // mov rdx, rsp
	WriteInstructionCall(0x5771CD, FnPtr(CWareHouse::DisassembleSoldItem));

	WriteInstructionCall(0x58C64C, FnPtr(CWareHouse::BuyItemsDisassembleItem));
	WriteInstructionCall(0x58C7F0, FnPtr(CWareHouse::BuyItemsMakeNewItem));
	WriteInstructionCall(0x58CCEF, FnPtr(CWareHouse::BuyItemsMakeNewItem));
	WriteInstructionCall(0x58CC10, FnPtr(CWareHouse::BuyItemsAssembleItem));
	WriteInstructionCall(0x58CF76, FnPtr(CWareHouse::BuyItemsAssembleItem));

	const static char *buyItemsAdenaItemHelper =
		/* 0x0000 */ "\x48\x8B\x8C\x24\xD8\x00\x00\x00"	// mov rcx, [rsp+0xD8] - adena item ptr
		/* 0x0008 */ "\x48\x8B\x94\x24\x98\x01\x00\x00"	// mov rdx, [rsp+0x198] - total price
		/* 0x0010 */ "\x4C\x8D\x84\x24\xA0\x01\x00\x00"	// lea r8, [rsp+0x1A0] - adena item id ptr
		/* 0x0018 */ "\x90\x90\x90\x90\x90"				// call helper
		/* 0x001D */ "\x48\x8B\x8C\x24\xD8\x00\x00\x00"	// mov rcx, [rsp+0xd8] - adena item ptr
		/* 0x0025 */ "\x45\x33\xED"						// xor r13, r13 - zero for later use
		/* 0x0028 */ "\x49\xC7\xC4\x02\x00\x00\x00"		// mov r12, 2 - for later use
		/* 0x002F */ "\xFF\xE0"							// jmp rax
		/* 0x0031 */ ;
	MakeExecutable(reinterpret_cast<UINT32>(buyItemsAdenaItemHelper), 0x31);
	WriteInstructionCall(reinterpret_cast<UINT32>(buyItemsAdenaItemHelper) + 0x18, FnPtr(CWareHouse::BuyItemsHelper));
	WriteInstructionJmp(0x58C1B8, reinterpret_cast<UINT32>(buyItemsAdenaItemHelper));
}

void CWareHouse::Initialize(int warehouseNo, bool loadItems)
{
	reinterpret_cast<void(*)(CWareHouse*)>(0x48B72C)(this);
	*reinterpret_cast<UINT64*>(this) = 0x5FBB88;
	reinterpret_cast<void(*)(char*)>(0x4B06D8)(reinterpret_cast<char*>(this) + 0x18);
	reinterpret_cast<void(*)(char*)>(0x44E168)(reinterpret_cast<char*>(this) + 0x50);
	reinterpret_cast<void(*)(CWareHouse*, int, bool)>(0x567308)(this, warehouseNo, loadItems);
}

int CWareHouse::AssembleSoldItem(char *buffer, int limit, void *rsp, int itemId, INT64 newAmount)
{
	// get adena from total adena variable (set in DisassembleSoldItem)
	INT64 adena = *reinterpret_cast<INT64*>(reinterpret_cast<char*>(rsp) + 0x168);

	// get sold amount from stack
	INT64 soldAmount = *reinterpret_cast<INT64*>(reinterpret_cast<char*>(rsp) + 0x190);

	// old format was dQ (itemId, newAmount)
	return Assemble(buffer, limit, "dQQQ", itemId, newAmount, adena, soldAmount);
}

const unsigned char* CWareHouse::DisassembleSoldItem(const unsigned char *buffer, void *rsp, int *itemId, INT64 *amount)
{
	// store adena to total adena variable - we don't need it anymore
	INT64 *adena = reinterpret_cast<INT64*>(reinterpret_cast<char*>(rsp) + 0x168);

	// old format was dQ (itemId, amount)
	return Disassemble(buffer, "dQQ", itemId, amount, adena);
}

const unsigned char* CWareHouse::BuyItemsDisassembleItem(const unsigned char *buffer, char *rsp, int *itemId, INT64 *amount, int *consumeType)
{
	BuySellExt *ext = &buySellExt[GetThreadIndex()];

	// old format was dQd
	const unsigned char *ret = Disassemble(buffer, "dQdddddhhhhhhhh", itemId, amount, consumeType,
		&ext->refundId, &ext->bless, &ext->enchant, &ext->damaged,
		&ext->attributeAttackType, &ext->attributeAttackValue,
		&ext->attributeFire, &ext->attributeWater,
		&ext->attributeWind, &ext->attributeEarth,
		&ext->attributeDivine, &ext->attributeDark);

	if (ext->refundId) {
		ScopedLock lock(CUser::alreadyRefundedItemsCS);
		std::map<UINT32, std::set<UINT32> >::const_iterator i = CUser::alreadyRefundedItems.find(ext->userId);
		if (i == CUser::alreadyRefundedItems.end()) {
			CLog::Add(CLog::Red, L"Can't find user in already refunded items map. Abort refund.");
			*itemId = 0;
		} else if (i->second.count(ext->refundId)) {
			CLog::Add(CLog::Red, L"User trying to refund item twice, hack? Abort refund.");
			*itemId = 0;
		}
	}

	return ret;
}

int CWareHouse::BuyItemsAssembleItem(char *buffer, int limit, char *rsp,
	UINT32 itemId, UINT32 itemTypeId, UINT64 amount, UINT32 enchant,
	UINT32 damaged, UINT32 bless, UINT32 ident, UINT32 wished, UINT32 unknown)
{
	BuySellExt *ext = &buySellExt[GetThreadIndex()];

	if (ext->refundId) {
		ScopedLock lock(CUser::alreadyRefundedItemsCS);
		std::map<UINT32, std::set<UINT32> >::iterator i = CUser::alreadyRefundedItems.find(ext->userId);
		if (i == CUser::alreadyRefundedItems.end()) {
			CLog::Add(CLog::Red, L"Can't find user in already refunded items map. Can't store already refunded item. THIS IS BAD!");
		} else if (!i->second.insert(ext->refundId).second) {
			CLog::Add(CLog::Red, L"User refunded item twice! THIS IS REALLY BAD!");
		}
	}

	// old format was ddQdddddd
	return Assemble(buffer, limit, "ddQddddddd",
		itemId, itemTypeId, amount, ext->enchant, ext->damaged, ext->bless,
		ident, wished, unknown, ext->refundId);
}

void* CWareHouse::BuyItemsMakeNewItem(
	CWareHouse *self, char *ptr, UINT32 userId, UINT32 itemType, INT64 amount,
	UINT32 enchant, UINT32 damaged, UINT32 bless, UINT32 unknown1, UINT32 unknown2, UINT32 warehouseNo,
	UINT32 *option, UINT32 unknown3, UINT16 *attribute)
{
	BuySellExt *ext = &buySellExt[GetThreadIndex()];

	enchant = ext->enchant;
	damaged = ext->damaged;
	bless = ext->bless;
	attribute = &ext->attributeAttackType;

	return reinterpret_cast<void*(*)(
		CWareHouse*, char*, UINT32, UINT32, INT64,
		UINT32, UINT32, UINT32, UINT32, UINT32, UINT32,
		UINT32*, UINT32, UINT16*)
	>(0x5598C8)(
		self, ptr, userId, itemType, amount, enchant, damaged, bless, unknown1, unknown2, warehouseNo,
		option, unknown3, attribute);
}

UINT64 CWareHouse::BuyItemsHelper(UINT64 adenaPtr, INT64 price, INT32 *adenaItemId)
{
	if (!price) {
		*adenaItemId = 0;
		return 0x58C5E4; // continue without messing with adena
	}
	if (adenaPtr) {
		return 0x58C1CF; // adena item present
	} else {
		return 0x58C1C5; // user has no adena
	}
}

SmartPtr<CItem> CWareHouse::GetItem(const int id)
{
	SmartPtr<CItem> ret;
	reinterpret_cast<void(*)(CWareHouse*, SmartPtr<CItem>*, int)>(0x56EED0)(
		this, &ret, id);
	return ret;
}

UINT64 CWareHouse::GetLockAddr()
{
	return reinterpret_cast<UINT64(*)(CWareHouse*)>(0x5542E8)(this);
}

void CWareHouse::ReadLock(const wchar_t *file, const int line)
{
	reinterpret_cast<void(*)(CWareHouse*, const wchar_t*, const int)>(0x55448C)(
		this, file, line);
}

void CWareHouse::ReadUnlock()
{
	reinterpret_cast<void(*)(CWareHouse*)>(0x554534)(this);
}

void CWareHouse::WriteLock(const wchar_t *file, const int line)
{
	reinterpret_cast<void(*)(CWareHouse*, const wchar_t*, const int)>(0x55434C)(
		this, file, line);
}

void CWareHouse::WriteUnlock()
{
	reinterpret_cast<void(*)(CWareHouse*)>(0x5543F4)(this);
}

CWareHouse::LockGuard::LockGuard(CWareHouse *warehouse, const bool writeLock) : writeLock(writeLock), warehouse(warehouse), locked(false)
{
}

CWareHouse::LockGuard::~LockGuard()
{
	Unlock();
}

void CWareHouse::LockGuard::Lock(const wchar_t *file, const int line)
{
	if (locked) return;
	if (writeLock) {
		warehouse->WriteLock(file, line);
	} else {
		warehouse->ReadLock(file, line);
	}
	locked = true;
}

void CWareHouse::LockGuard::Unlock()
{
	if (!locked) return;
	if (writeLock) {
		warehouse->WriteUnlock();
	} else {
		warehouse->ReadUnlock();
	}
	locked = false;
}

CWareHouse::LockGuardN::LockGuardN()
	: locked(false)
{
}

CWareHouse::LockGuardN::~LockGuardN()
{
	Unlock();
}

void CWareHouse::LockGuardN::Add(CWareHouse *warehouse)
{
	UINT64 lockAddr = warehouse->GetLockAddr();
	warehouses.insert(std::make_pair(lockAddr, warehouse));
}

void CWareHouse::LockGuardN::Lock(const wchar_t *file, const int line)
{
	if (locked) return;
	for (std::map<UINT64, CWareHouse*>::iterator i = warehouses.begin() ; i != warehouses.end() ; ++i) {
		i->second->WriteLock(file, line);
	}
	locked = true;
}

void CWareHouse::LockGuardN::Unlock()
{
	if (!locked) return;
	for (std::map<UINT64, CWareHouse*>::reverse_iterator i = warehouses.rbegin() ; i != warehouses.rend() ; ++i) {
		i->second->WriteUnlock();
	}
	locked = false;
}

CWareHouse::LockGuard2::LockGuard2(CWareHouse *warehouse1, CWareHouse *warehouse2)
{
	Add(warehouse1);
	Add(warehouse2);
}

CWareHouse::LockGuard3::LockGuard3(CWareHouse *warehouse1, CWareHouse *warehouse2, CWareHouse *warehouse3)
{
	Add(warehouse1);
	Add(warehouse2);
	Add(warehouse3);
}

CWareHouse::TransactionGuard::TransactionGuard() : done(false)
{
}

CWareHouse::TransactionGuard::~TransactionGuard()
{
	if (!done) {
		CLog::Add(CLog::Red, L"[NO ERROR] Automatically rolling back all pending transactions");
		Rollback();
	}
	for (std::list<Item*>::iterator i = transactions.begin() ; i != transactions.end() ; ++i) {
		delete (*i);
	}
}

void CWareHouse::TransactionGuard::Commit()
{
	if (done) {
		CLog::Add(CLog::Red, L"TransactionGuard: already done");
		return;
	}
	for (std::list<Item*>::iterator i = transactions.begin() ; i != transactions.end() ; ++i) {
		(*i)->original->CommitTransaction(&*(*i)->backup, true);
		(*i)->Log();
	}
	done = true;
}

void CWareHouse::TransactionGuard::Rollback()
{
	if (done) {
		CLog::Add(CLog::Red, L"TransactionGuard: already done");
		return;
	}
	for (std::list<Item*>::iterator i = transactions.begin() ; i != transactions.end() ; ++i) {
		if (!(*i)->original) continue;
		(*i)->original->RollbackTransaction(&*(*i)->backup);
	}
	done = true;
}

CWareHouse::TransactionGuard::Item& CWareHouse::TransactionGuard::Create(SmartPtr<CItem> item)
{
	Item *ret = new Item(*this, item, CItem::CREATE);
	transactions.push_back(ret);
	return *ret;
}

CWareHouse::TransactionGuard::Item& CWareHouse::TransactionGuard::Update(SmartPtr<CItem> item)
{
	Item *ret = new Item(*this, item, CItem::UPDATE);
	transactions.push_back(ret);
	return *ret;
}

CWareHouse::TransactionGuard::Item::Item(TransactionGuard &guard, SmartPtr<CItem> item, CItem::TransactionMode mode)
	: original(item)
{
	if (!original) return;
	original->SetTransaction();
	backup = original->Copy();
	backup->SetTransactMode(mode);
}

CWareHouse::TransactionGuard::Item::operator bool()
{
	return bool(original);
}

CWareHouse::TransactionGuard::Item::operator SmartPtr<CItem>()
{
	return original;
}

CItem* CWareHouse::TransactionGuard::Item::operator->()
{
	return &*original;
}

CItem& CWareHouse::TransactionGuard::Item::operator*()
{
	return *original;
}

void CWareHouse::TransactionGuard::Item::AddAudit(int eventId, CUser *user1, CUser *user2, CItem *item, INT64 change, INT64 newAmount)
{
	AuditInfo info;
	info.eventId = eventId;
	info.user1 = user1;
	info.user2 = user2;
	info.item = item;
	info.change = change;
	info.newAmount = newAmount;
	auditInfo.push_back(info);
}

void CWareHouse::TransactionGuard::Item::Log() const
{
	for (std::list<AuditInfo>::const_iterator i = auditInfo.begin() ; i != auditInfo.end() ; ++i) {
		i->Log();
	}
}

void CWareHouse::TransactionGuard::Item::AuditInfo::Log() const
{
	CLog::Add(CLog::In, L"%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,,%d:%d:%d:%d:%d:%d:%d:%d:%d,,%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,%I64d,%s,%s,%s,%s,%I64d",
		INT64(eventId), INT64(user1->dbId), INT64(user1->GetAccountID()), INT64(user2 ? user2->dbId : 0),
		INT64(user2 ? user2->GetAccountID() : 0), INT64(user1->GetX()), INT64(user1->GetY()), INT64(user1->GetZ()),
		item->attribute[0], item->attribute[1], item->attribute[2], item->attribute[3], item->attribute[4],
		item->attribute[5], item->attribute[6], item->attribute[7], item->attribute[8],
		INT64(user1->GetRace()), INT64(user1->GetGender()), INT64(user1->GetClass()), INT64(user1->GetLevel()),
		INT64(user2 ? user2->GetClass() : 0), INT64(user2 ? user2->GetLevel() : 0), INT64(item->enchant),
		INT64(item->ItemType()), change, newAmount, user1->GetCharName(), user1->GetAccountName(),
		user2 ? user2->GetCharName() : L"", user2 ? user2->GetAccountName() : L"", item->GetDBID());
}

bool CWareHouse::AddAdena(INT64 amount)
{
	return reinterpret_cast<bool(*)(CWareHouse*, INT64)>(0x56D820)(this, amount);
}

bool CWareHouse::DelAdena(INT64 amount)
{
	return reinterpret_cast<bool(*)(CWareHouse*, INT64)>(0x56D908)(this, amount);
}

bool CWareHouse::PushItem(CItem *item)
{
	return reinterpret_cast<bool(*)(CWareHouse*, CItem*)>(0x5704EC)(this, item);
}

SmartPtr<CItem> CWareHouse::PopItem(int id)
{
	SmartPtr<CItem> ret;
	reinterpret_cast<void(*)(CWareHouse*, SmartPtr<CItem>*, int)>(0x57038C)(this, &ret, id);
	return ret;
}

SmartPtr<class CItem> CWareHouse::MakeNewItem(int ownerId, int itemType, INT64 amount, int warehouseNo)
{
	SmartPtr<class CItem> ret;
	int options[2];
	memset(options, 0, sizeof(options));
	INT16 attributes[9];
	memset(attributes, 0, sizeof(attributes));
	attributes[0] = -2;
	reinterpret_cast<void(*)(CWareHouse*, SmartPtr<class CItem>*, int, int, INT64,
		int, int, int, int, int, int, int*, int, INT16*)>(0x5598C8)(this, &ret, ownerId, itemType, amount,
			0, 0, 0, 0, 0, warehouseNo, options, 0, attributes);
	return ret;
}

int CWareHouse::GetWarehouseNo() const
{
	return warehouseNo;
}

void CWareHouse::LoadItems(const int warehouseNo)
{
	reinterpret_cast<void(*)(CWareHouse*, const int)>(0x55940C)(this, warehouseNo);
}

SmartPtr<CItem> CWareHouse::GetAdena()
{
	SmartPtr<CItem> ret;
	reinterpret_cast<void(*)(CWareHouse*, SmartPtr<CItem>*)>(0x56BD54)(this, &ret);
	return ret;
}

SmartPtr<CItem> CWareHouse::GetItemByType(const int type, const int ident)
{
	SmartPtr<CItem> ret;
	reinterpret_cast<void(*)(CWareHouse*, SmartPtr<CItem>*, const int, const int)>(0x56B2A8)(this, &ret, type, ident);
	return ret;
}

CompileTimeOffsetCheck(CWareHouse, warehouseNo, 0xAC);
CompileTimeOffsetCheck(CWareHouse, padding0x00B0, 0xB0);

} // namespace Cached

