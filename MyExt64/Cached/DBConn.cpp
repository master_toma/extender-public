
#include <Cached/DBConn.h>
#include <Common/CLog.h>
#include <sqltypes.h>
#include <sqlext.h>

DBConn::DBConn() : preparedQuery(0)
{
	reinterpret_cast<DBConn*(*)(DBConn*, int)>(0x454660)(this, 0);
}

DBConn::~DBConn()
{
	reinterpret_cast<void(*)(DBConn*)>(0x452A74)(this);
}

void DBConn::Bind(int *i)
{
	reinterpret_cast<void(*)(DBConn*, int*)>(0x453170)(this, i);
}

void DBConn::Bind(UINT32 *i)
{
	reinterpret_cast<void(*)(DBConn*, UINT32*)>(0x4530D0)(this, i);
}

void DBConn::Bind(INT64 *i)
{
	reinterpret_cast<void(*)(DBConn*, INT64*)>(0x453210)(this, i);
}

void DBConn::Bind(wchar_t *buffer, const size_t sizeInBytes)
{
	reinterpret_cast<void(*)(DBConn*, wchar_t*, const size_t)>(0x452F94)(
		this, buffer, sizeInBytes);
}

void DBConn::Unbind()
{
	reinterpret_cast<void(*)(DBConn*)>(0x45347C)(this);
}

void DBConn::Prepare(const wchar_t *query)
{
	SQLPrepareW(handle, const_cast<SQLWCHAR*>(query), SQL_NTS);
	preparedQuery = query;
}

void DBConn::BindParameter(const UINT32 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_ULONG, SQL_INTEGER, 4, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

void DBConn::BindParameter(const INT32 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 4, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

void DBConn::BindParameter(const INT64 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_SBIGINT, SQL_BIGINT, 8, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

void DBConn::BindParameter(const UINT64 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_UBIGINT, SQL_BIGINT, 8, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

bool DBConn::Execute()
{
	return reinterpret_cast<bool(*)(DBConn*, const wchar_t*)>(0x4539BC)(
		this, preparedQuery);
}

bool DBConn::Fetch()
{
	return reinterpret_cast<bool(*)(DBConn*)>(0x452B4C)(this);
}

