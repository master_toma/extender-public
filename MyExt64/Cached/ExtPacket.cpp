
#include <Cached/ExtPacket.h>
#include <Cached/CSocket.h>
#include <Cached/MailSystem.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Server/CDB.h>

namespace Cached {

void ExtPacket::Init()
{
	WriteInstructionJmp(0x4DB4BC, FnPtr(Handler));
}

bool ExtPacket::Handler(CSocket *socket, const unsigned char *packet)
{
	GUARDED;

	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	bool(*handler)(CSocket*, const unsigned char*);
	if (opcode > 0x33) {
		handler = reinterpret_cast<bool(**)(CSocket*, const unsigned char*)>(0x191D2D8)[0];
	} else if (opcode == 0x33) {
		handler = &ExtHandler;
	} else {
		handler = reinterpret_cast<bool(**)(CSocket*, const unsigned char*)>(0x191D2E0)[opcode];
	}
	return handler(socket, packetData);
}

bool ExtPacket::ExtHandler(CSocket *socket, const unsigned char *packet)
{
	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	switch (opcode) {
	case CDB::REQUEST_TEST: return TestPacket(socket, packetData); break;
	case CDB::REQUEST_SEND_POST: return SendPostPacket(socket, packetData); break;
	case CDB::REQUEST_RECEIVE_POST_LIST: return ReceivePostListPacket(socket, packetData); break;
	case CDB::REQUEST_SENT_POST_LIST: return SentPostListPacket(socket, packetData); break;
	case CDB::REQUEST_RECEIVED_POST: return ReceivedPostPacket(socket, packetData); break;
	case CDB::REQUEST_SENT_POST: return SentPostPacket(socket, packetData); break;
	case CDB::REQUEST_DELETE_RECEIVED_POST: return DeleteReceivedPostPacket(socket, packetData);
	case CDB::REQUEST_DELETE_SENT_POST: return DeleteSentPostPacket(socket, packetData);
	case CDB::REQUEST_RECEIVE_POST: return ReceivePostPacket(socket, packetData);
	case CDB::REQUEST_CHECK_MAIL: return CheckMailPacket(socket, packetData);
	case CDB::REQUEST_REJECT_POST: return RejectPostPacket(socket, packetData);
	case CDB::REQUEST_CANCEL_POST: return CancelPostPacket(socket, packetData);
	case CDB::REQUEST_MAIL_UPKEEP: return MailUpkeepPacket(socket, packetData);
	default:
		CLog::Add(CLog::Red, L"Unknown ExtPacket opcode %04X", opcode);
		return false;
	}
}

bool ExtPacket::TestPacket(CSocket *socket, const unsigned char *packet)
{
	CLog::Add(CLog::Blue, L"TestPacket :)");
	socket->Send("chh", 0xD2, 0x0077, CDB::REPLY_TEST);
	return false;
}

bool ExtPacket::SendPostPacket(CSocket *socket, const unsigned char *packet)
{
	MailSystem::SendPost(socket, MailSystem::Post::FromPacket(&packet));
	return false;
}

bool ExtPacket::ReceivePostListPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	Disassemble(packet, "dd", &userDbId, &userObjectId);
	MailSystem::ReceivePostList(socket, userDbId, userObjectId);
	return false;
}

bool ExtPacket::SentPostListPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	Disassemble(packet, "dd", &userDbId, &userObjectId);
	MailSystem::SentPostList(socket, userDbId, userObjectId);
	return false;
}

bool ExtPacket::ReceivedPostPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	int id = 0;
	Disassemble(packet, "ddd", &userDbId, &userObjectId, &id);
	MailSystem::ReceivedPost(socket, userDbId, userObjectId, id);
	return false;
}

bool ExtPacket::SentPostPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	int id = 0;
	Disassemble(packet, "ddd", &userDbId, &userObjectId, &id);
	MailSystem::SentPost(socket, userDbId, userObjectId, id);
	return false;
}

bool ExtPacket::DeleteReceivedPostPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	int count = 0;
	packet = Disassemble(packet, "ddd", &userDbId, &userObjectId, &count);
	std::vector<int> ids;
	for (size_t i = 0 ; i < count ; ++i) {
		int id = 0;
		packet = Disassemble(packet, "d", &id);
		ids.push_back(id);
	}
	MailSystem::DeleteReceivedPost(socket, userDbId, userObjectId, ids);
	return false;
}

bool ExtPacket::DeleteSentPostPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	int count = 0;
	packet = Disassemble(packet, "ddd", &userDbId, &userObjectId, &count);
	std::vector<int> ids;
	for (size_t i = 0 ; i < count ; ++i) {
		int id = 0;
		packet = Disassemble(packet, "d", &id);
		ids.push_back(id);
	}
	MailSystem::DeleteSentPost(socket, userDbId, userObjectId, ids);
	return false;
}

bool ExtPacket::ReceivePostPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	int id = 0;
	Disassemble(packet, "ddd", &userDbId, &userObjectId, &id);
	MailSystem::ReceivePost(socket, userDbId, userObjectId, id);
	return false;
}

bool ExtPacket::CheckMailPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	Disassemble(packet, "dd", &userDbId, &userObjectId);
	MailSystem::CheckMail(socket, userDbId, userObjectId);
	return false;
}

bool ExtPacket::RejectPostPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	int id = 0;
	Disassemble(packet, "ddd", &userDbId, &userObjectId, &id);
	MailSystem::RejectPost(socket, userDbId, userObjectId, id);
	return false;
}

bool ExtPacket::CancelPostPacket(class CSocket *socket, const unsigned char *packet)
{
	int userDbId = 0;
	int userObjectId = 0;
	int id = 0;
	Disassemble(packet, "ddd", &userDbId, &userObjectId, &id);
	MailSystem::CancelPost(socket, userDbId, userObjectId, id);
	return false;
}

bool ExtPacket::MailUpkeepPacket(class CSocket *socket, const unsigned char *packet)
{
	MailSystem::Upkeep(socket);
	return false;
}

} // namespace Cached

