
#include <NPCd/CCreature.h>

namespace NPC {

CCreature* CCreature::GetObject(const UINT32 objectId)
{
	return reinterpret_cast<CCreature*(*)(UINT64, UINT32)>(0x423E68)(
		0x7690F0, objectId);
}

} // namespace NPC

