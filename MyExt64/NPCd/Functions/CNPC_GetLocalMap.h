
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>

class CNPC_GetLocalMap : public NPCFunction {
public:
	CNPC_GetLocalMap();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int GetLocalMap(CNPC *npc, int mapId, int key);
};

