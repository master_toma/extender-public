
#include <NPCd/Functions/CNPC_ClearContributeData.h>
#include <NPCd/NPCd.h>
#include <Common/CLog.h>

CNPC_ClearContributeData::CNPC_ClearContributeData() :
	NPCFunction(L"ClearContributeData", &ClearContributeData)
{
}

void* CNPC_ClearContributeData::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*)>(functionPtr.functionPtr)(
		caller);
}

void CNPC_ClearContributeData::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
}

int CNPC_ClearContributeData::ClearContributeData(CNPC *npc)
{
	NPCd::Send("chd", 0x36, NPCd::CLEAR_CONTRIBUTE_DATA, npc->sm->index);
	return 0;
}

