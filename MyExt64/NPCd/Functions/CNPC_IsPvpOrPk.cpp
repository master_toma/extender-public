
#include <NPCd/Functions/CNPC_IsPvpOrPk.h>
#include <Common/CLog.h>

CNPC_IsPvpOrPk::CNPC_IsPvpOrPk() :
	NPCFunction(L"IsPvpOrPk", &IsPvpOrPk)
{
}

void* CNPC_IsPvpOrPk::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*)>(functionPtr.functionPtr)(
		caller, params[0]);
}

void CNPC_IsPvpOrPk::SetTypes()
{
	SetReturnType(Type::TYPE_INT);
	AddParameter(Type::TYPE_CREATURE);
}

int CNPC_IsPvpOrPk::IsPvpOrPk(CNPC *npc, CSharedCreatureData *c)
{
	if (!c) return 0;
	if (c->karma) return 2;
	if (c->isFlagged) return 1;
	return 0;
}

