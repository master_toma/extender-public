
#include <NPCd/CNpcSpawnDefine.h>
#include <NPCd/StaticRespawn.h>
#include <Common/Utils.h>
#include <Common/CLog.h>

void CNpcSpawnDefine::Init()
{
	WriteInstructionCall(0x422C5F, FnPtr(&CNpcSpawnDefine::GetRespawnTime));
	WriteInstructionCall(0x510952, FnPtr(&CNpcSpawnDefine::GetRespawnTime));
	WriteInstructionCall(0x520925, FnPtr(&CNpcSpawnDefine::GetRespawnTime));
	WriteInstructionCall(0x54A8C0, FnPtr(&CNpcSpawnDefine::GetRespawnTime));
}

int CNpcSpawnDefine::GetRespawnTime()
{
	std::map<std::wstring, StaticRespawn::StaticRespawnDef>::const_iterator idefs = StaticRespawn::defs.find(name);
	if (idefs == StaticRespawn::defs.end()) return reinterpret_cast<int(*)(CNpcSpawnDefine*)>(0x54A400)(this);
	time_t now = time(0);
	time_t minimumSpawnTime = now + idefs->second.minimumDelaySeconds;
	struct tm t;
	localtime_s(&t, &minimumSpawnTime);
	t.tm_hour = idefs->second.respawnTimeHour;
	t.tm_min = idefs->second.respawnTimeMinute;
	time_t respawnWindowStartTime = mktime(&t) - idefs->second.respawnTimeRandomSeconds / 2;
	if (respawnWindowStartTime < minimumSpawnTime) respawnWindowStartTime += 86400;
	time_t respawnTime = respawnWindowStartTime - now;
	time_t result = respawnTime + RandomDouble() * idefs->second.respawnTimeRandomSeconds;
	return result;
}

