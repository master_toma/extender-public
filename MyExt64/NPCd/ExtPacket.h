
#pragma once

class ExtPacket {
public:
	static void Init();
	static void Register();
	static bool Handler(void *socket, unsigned char *packet);

	static bool IsToggleSkillOnOff(void *socket, const unsigned char *packet);

	const static int IS_TOGGLE_SKILL_ONOFF = 1;
};

