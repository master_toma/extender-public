
#include <NPCd/NPCd.h>
#include <NPCd/SplitAI.h>
#include <NPCd/Compiler.h>
#include <NPCd/CNPC.h>
#include <NPCd/CNPCMaker.h>
#include <NPCd/NPCFunction.h>
#include <NPCd/NPCHandler.h>
#include <NPCd/CFString.h>
#include <NPCd/ExtPacket.h>
#include <NPCd/StaticRespawn.h>
#include <NPCd/CNpcSpawnDefine.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CSharedItemData.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/Config.h>

void NPCd::Init()
{
	DisableSendMail();
	HookStart();
	ChangePaths();
	if (Config::Instance()->npcd->useSplitAI) {
		SplitAI::Init();
	}
	CSharedCreatureData::InitNpc();
	Compiler::Init();
	CNPC::Init();
	CNPCMaker::Init();
	NPCFunction::Init();
	NPCHandler::Init();
	CFString::Init();
	ExtPacket::Init();
	InitUtf8Support();
	CNpcSpawnDefine::Init();
	FixWcstol();
	NOPMemory(0x47137A, 6); // NOP out SetThreadAffinityMask
}

void NPCd::DisableSendMail()
{
	NOPMemory(0x42CD41, 5);
	NOPMemory(0x42CDBE, 5);
}

void NPCd::HookStart()
{
	WriteInstructionCall(0x4759C7, reinterpret_cast<UINT32>(StartHook));
	WriteInstructionCall(0x474D11, reinterpret_cast<UINT32>(CreateWindowEx), 0x474D11 + 6);
}

void NPCd::ChangePaths()
{
	ReplaceString(0x649CA8, L"IMPORT", L"script");
	ReplaceString(0x649C70, L"IMPORT", L"script");
	ReplaceString(0x649E50, L"IMPORT", L"script");
}

HWND NPCd::CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
{
	std::wstring name(lpWindowName);
	if (!Compiler::filename.empty()) {
		name += L" - NASC COMPILE MODE";
	}
	name += L" - patched by MyExt64 (https://bitbucket.org/l2shrine/extender-public)";
	X = 100;
	Y = 100;
	return ::CreateWindowEx(dwExStyle, lpClassName, name.c_str(), dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
}

void NPCd::StartHook(void *logger, int level, const char *fmt)
{ GUARDED

	if (Compiler::filename.empty()) {
		reinterpret_cast<void(*)(void*, int, const char*)>(0x478038)(logger, level, fmt);
	}
	CLog::Add(CLog::Blue, L"Patched by MyExt64 (https://bitbucket.org/l2shrine/extender-public)");
	CLog::Add(CLog::Blue, L" ");
	CLog::Add(CLog::Blue, L"To use l2npc.exe as a NASC compiler, run it with parameters:");
	CLog::Add(CLog::Blue, L" ");
	if (Compiler::exeFilename.size() < 2048) {
		CLog::Add(CLog::Blue, L"    %s [-c|--close] [-e|--close-on-error] FILENAME", Compiler::exeFilename.c_str());
	} else {
		CLog::Add(CLog::Blue, L"    %s [-c|--close] [-e|--close-on-error] FILENAME", Compiler::exeFilename.substr(0, 2048).c_str());
	}
	CLog::Add(CLog::Blue, L" ");
	CLog::Add(CLog::Blue, L"    -c, --close           Close after successful compilation");
	CLog::Add(CLog::Blue, L"    -e, --close-on-error  Close after unsuccessful compilation");
	CLog::Add(CLog::Blue, L"    FILENAME              NASC file to compile");
	CLog::Add(CLog::Blue, L" ");

	if (Compiler::filename.empty()) {
		ShellExecute(0, L"open", L"cmd.exe", L"/C mkdir bak", 0, SW_HIDE);
		ShellExecute(0, L"open", L"cmd.exe", L"/C move LinError.txt.*.bak bak\\", 0, SW_HIDE);
	}

	StaticRespawn::Load();
}

void NPCd::Send(const char *format, ...)
{
	va_list va;
	va_start(va, format);
	SendV(format, va);
	va_end(va);
}

void NPCd::SendV(const char *format, va_list va)
{
	reinterpret_cast<void(*)(UINT64, const char*, va_list)>(0x5473CC)(0x1C1F080, format, va);
}

namespace {

template<UINT32 consumeBomAddress>
int GetCharForLexer(void **stream)
{
	static bool utf8 = false;
	void *obj = stream[14];
	if (!obj) return -1;
	int *consumeBom = reinterpret_cast<int*>(consumeBomAddress);
	bool characterAlreadyConsumed = false;
	unsigned char c1 = 0;
	if (*consumeBom) {
		*consumeBom = 0;
		c1 = reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj);
		{
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) return -1;
		}
		if (c1 == 0xFF) {
			if (reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj) != 0xFE) {
				CLog::Add(CLog::Blue, L"wrong utf-16le bom");
				return -1;
			}
			utf8 = false;
		} else if (c1 == 0xEF) {
			if (reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj) != 0xBB) {
				CLog::Add(CLog::Blue, L"wrong utf-8 bom (1)");
				return -1;
			}
			{
				bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
				if (*fail) return -1;
			}
			if (reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj) != 0xBF) {
				CLog::Add(CLog::Blue, L"wrong utf-8 bom (2)");
				return -1;
			}
			{
				bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
				if (*fail) return -1;
			}
			utf8 = true;
		} else {
			utf8 = true;
			characterAlreadyConsumed = true;
		}
	}
	if (!utf8) {
		unsigned char c1 = reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj);
		{
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) return -1;
		}
		unsigned char c2 = reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj);
		{
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) return -1;
		}
		return c1 | (c2 << 8);
	} else {
		unsigned char c;
		if (characterAlreadyConsumed) {
			c = c1;
		} else {
			c = reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj);
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) {
				return -1;
			}
		}
		if (c < 0x80) return c;
		if (c < 0xC0) {
			CLog::Add(CLog::Red, L"Can't decode UTF-8 (invalid character)");
			return -1;
		}
		size_t chars(0);
		wchar_t wc(0);
		if (c >= 0xFC) {
			wc = c & 0x01;
			chars = 5;
		} else if (c >= 0xF8) {
			wc = c & 0x03;
			chars = 4;
		} else if (c >= 0xF0) {
			wc = c & 0x07;
			chars = 3;
		} else if (c >= 0xE0) {
			wc = c & 0x0F;
			chars = 2;
		} else {
			wc = c & 0x1F;
			chars = 1;
		}
		for (; chars; --chars) {
			wc <<= 6;
			c = reinterpret_cast<wchar_t(*)(void*)>(0x5CA360)(obj);
			wc |= c & 0x3f;
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) {
				CLog::Add(CLog::Red, L"Can't decode UTF-8 (end of input)");
				return -1;
			}
		}
		return wc;
	}
}

} // namespace

void NPCd::InitUtf8Support()
{
	WriteInstructionCall(0x5B9B0D, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x5B9BF3, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x5B71AD, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x5BA517, FnPtr(LoadBinaryAbsolute));
	if (!Config::Instance()->npcd->useSplitAI) {
		WriteInstructionCall(0x5CBA17, FnPtr(LoadBinaryAbsolute));
	}
	WriteInstructionCall(0x4755BA, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x559AD2, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x559B17, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x55A745, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x55AA52, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x55AC01, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x55AF65, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x5B899E, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x5B89B8, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x5B89E4, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x5B8C8C, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x5B8CC8, FnPtr(ReadNextWchar));
	WriteMemoryQWORD(0x6DAD98, FnPtr(GetCharForLexer<0x7666EC>));
	WriteMemoryQWORD(0x6DB2C8, FnPtr(GetCharForLexer<0x766718>));
	WriteMemoryQWORD(0x6DDCE8, FnPtr(GetCharForLexer<0x766778>));
}

void NPCd::FixWcstol()
{
	WriteInstructionCall(0x4B594C + 0x3E0, FnPtr(WcstolFix));
	WriteInstructionCall(0x559A2C + 0x1CF, FnPtr(WcstolFix));
	WriteInstructionCall(0x5C9DBC + 0x4F, FnPtr(WcstolFix));
	WriteInstructionJmp(0x5EAA66, FnPtr(WcstolFix));
	WriteInstructionJmp(0x5EAA86, FnPtr(WcstolFix));
	WriteInstructionCall(0x6061E6, FnPtr(WcstolFix));
	WriteInstructionCall(0x60627F, FnPtr(WcstolFix));
	WriteInstructionCall(0x606464, FnPtr(WcstolFix));
	WriteInstructionCall(0x6065FA, FnPtr(WcstolFix));
}