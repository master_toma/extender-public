
#pragma once

#include <Common/Utils.h>

class CSharedItemData {
public:
	/* 0x0000 */ void *unknown;
	/* 0x0008 */ double x;
	/* 0x0010 */ double y;
	/* 0x0018 */ double z;
	/* 0x0020 */ unsigned char padding0x0020[0x0024 - 0x0020];
	/* 0x0024 */ UINT32 objectId;
	/* 0x0028 */ int index;
	/* 0x002C */ unsigned char padding0x002C[0x0050 - 0x002C];
	/* 0x0050 */ bool pickable;
	/* 0x0051 */ unsigned char padding0x0051[0x0068 - 0x0051];
	/* 0x0068 */ UINT64 count;
	/* 0x0070 */ unsigned char padding0x0070[0x0078 - 0x0070];
	/* 0x0078 */ UINT32 classId;
	/* 0x007C */ UINT32 itemType2;
	/* 0x0080 */ UINT32 bodyPart;
	/* 0x0084 */ UINT32 bless;
	/* 0x0088 */ UINT32 consumeType;
	/* 0x008C */ UINT32 damaged;
	/* 0x0090 */ UINT32 enchant;
	/* 0x0094 */ unsigned char padding0x0094[0x0100 - 0x0094];
};

