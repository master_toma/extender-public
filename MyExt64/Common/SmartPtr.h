
#pragma once

#include <Common/CLog.h>
#include <windows.h>

template<class T>
class SmartPtr {
public:
	SmartPtr() :
		obj(0),
		type(0)
	{
	}

	__forceinline SmartPtr(T *obj, int type) :
		obj(obj),
		type(type)
	{
		if (obj) {
			IncRef(__FILE__, __LINE__);
		}
	}

	static SmartPtr<T> FromObjectID(UINT32 objectId)
	{
		SmartPtr<T> ret;
		ret.GetObjectByID(objectId);
		return ret;
	}

	static SmartPtr<T> FromIndex(UINT32 index)
	{
		SmartPtr<T> ret;
		ret.GetObjectByIndex(index);
		return ret;
	}

	static SmartPtr<T> FromDBID(UINT32 dbId)
	{
		SmartPtr<T> ret;
		ret.GetObjectByDBID(dbId);
		return ret;
	}

	__forceinline SmartPtr(const SmartPtr &other) :
		obj(other.obj),
		type(other.type)
	{
		if (obj) {
			IncRef(__FILE__, __LINE__);
		}
	}

	__forceinline ~SmartPtr()
	{
		if (obj) {
			DecRef(__FILE__, __LINE__);
		}
	}

	__forceinline SmartPtr& operator=(const SmartPtr &other)
	{
		if (obj) {
			DecRef(__FILE__, __LINE__);
		}
		obj = other.obj;
		type = other.type;
		if (obj) {
			IncRef(__FILE__, __LINE__);
		}
		return *this;
	}

	__forceinline SmartPtr& Reset(T *obj_, UINT32 type_)
	{
		if (obj) {
			DecRef(__FILE__, __LINE__);
		}
		obj = obj_;
		type = type_;
		if (obj) {
			IncRef(__FILE__, __LINE__);
		}
	}

	T* GetObjectByIndex(UINT32 index)
	{
		reinterpret_cast<void*(*)(void*, SmartPtr*, UINT32)>(0x448F14)(
			reinterpret_cast<void*>(0x10DE4580),
			this,
			index);
		return obj;
	}

	T* GetObjectByDBID(UINT32 dbId)
	{
		reinterpret_cast<void*(*)(void*, SmartPtr*, UINT32)>(0x44FE5C)(
			reinterpret_cast<void*>(0x11FDCA40),
			this,
			dbId);
		return obj;
	}

	T* GetObjectByID(UINT32 objectId)
	{
		if (obj) {
			DecRef(__FILE__, __LINE__);
		}
		obj = reinterpret_cast<T*(*)(void*, UINT32)>(0x41A3A4)(
			reinterpret_cast<void*>(0xF1B250),
			objectId);
		type = 9;
		if (obj) {
			IncRef(__FILE__, __LINE__);
		}
		return obj;
	}

	T& operator*()
	{
		return *obj;
	}

	T* operator->()
	{
		return obj;
	}

	operator bool()
	{
		return obj;
	}

protected:
	void IncRef(const char *file, const int line)
	{
		reinterpret_cast<void(*)(T*, const char*, int, UINT32)>(
			(*reinterpret_cast<void***>(obj))[1])(obj, file, line, type);
	}

	void DecRef(const char *file, const int line)
	{
		reinterpret_cast<void(*)(T*, const char*, int, UINT32)>(
			(*reinterpret_cast<void***>(obj))[2])(obj, file, line, type);
	}

	T *obj;
	UINT32 type;
};

