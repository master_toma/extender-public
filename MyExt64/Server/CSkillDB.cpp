
#include <Server/CSkillDB.h>

CSkillKey::CSkillKey(const INT16 skillId, const INT16 level) : skillId(skillId), level(level)
{
}

CSkillDB::CSkillDB()
{
}

CSkillDB::CSkillDB(const CSkillDB &other)
{
	(void) other;
}

CSkillDB& CSkillDB::operator=(const CSkillDB &other)
{
	(void) other;
	return *this;
}

CSkillDB* CSkillDB::Instance()
{
	return reinterpret_cast<CSkillDB*>(0x10F69440);
}

class CSkillInfo* CSkillDB::GetSkillInfo(int skillId, int level, class CCreature *creature)
{
	CSkillKey key(skillId, level);
	return reinterpret_cast<class CSkillInfo*(*)(CSkillDB*, CSkillKey&, class CCreature*)>(0x8216C0)(this, key, creature);
}

