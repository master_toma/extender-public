
#include <Server/CPartyRoomRegistry.h>
#include <Server/CUser.h>
#include <Common/Utils.h>

CPartyRoomRegistry* CPartyRoomRegistry::Instance()
{
	CPartyRoomRegistry *instance = reinterpret_cast<CPartyRoomRegistry*>(0xE467B30);
	int *initialized = reinterpret_cast<int*>(0xE467BB8);
	if (!*initialized) {
		*initialized |= 1;
		reinterpret_cast<void(*)(CPartyRoomRegistry)>(instance);
		atexit(reinterpret_cast<void(*)()>(0xA31BEC));
	}
	return instance;
}

bool CPartyRoomRegistry::IsPartyRoomMaster(SmartPtr<CUser> user)
{
	return reinterpret_cast<bool(*)(CPartyRoomRegistry*, SmartPtr<CUser>*)>(0x7A17CC)(this, &user);
}

bool CPartyRoomRegistry::IsPartyRoomMember(SmartPtr<CUser> user)
{
	return GetVfn<bool(*)(CPartyRoomRegistry*, SmartPtr<CUser>*)>(this, 1)(this, &user);
}

SmartPtr<CPartyRoom> CPartyRoomRegistry::FindByMember(SmartPtr<CUser> user)
{
	SmartPtr<CPartyRoom> result;
	reinterpret_cast<void(*)(CPartyRoomRegistry*, SmartPtr<CPartyRoom>*, SmartPtr<CUser>*)>(0x79EEF4)(this, &result, &user);
	return result;
}

