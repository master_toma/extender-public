
#pragma once

#include <Common/SmartPtr.h>

class CUser;

class CPartyRoomInfo {
public:
	SmartPtr<CUser> GetMaster();
};

