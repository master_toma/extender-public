
#pragma once

#include <Common/CYieldLock.h>
#include <list>
#include <utility>

class CInventory {
public:
	class ItemFunctor {
	public:
		virtual bool Check(class CItem *item) = 0;
	};

	class ListFillingItemFunctor : public ItemFunctor {
	public:
		ListFillingItemFunctor(std::list<CItem*> &list) : list(list) { }

		virtual bool Check(class CItem *item);
		virtual bool CheckWhetherAdd(class CItem *item) = 0;

	protected:
		std::list<CItem*> &list;
	};

	class ListFillingItemFunctorTradable : public ListFillingItemFunctor {
	public:
		ListFillingItemFunctorTradable(std::list<CItem*> &list, class CUser *user)
			: ListFillingItemFunctor(list), user(user) { }

		virtual bool CheckWhetherAdd(class CItem *item);

	protected:
		class CUser *user;
	};

	enum InventoryChange {
		CREATE = 1,
		UPDATE = 2,
		REMOVE = 3
	};

	static void Init();

	static class CItem* GetByServerIDIgnoreMoreStacks(CInventory *self, int id);
	static INT64 GetAdenaAmountWrapper(CInventory *self);
	static class CItem* GetFirstItemByClassIDIgnoreMoreStacks(CInventory *self, int classId, bool b);

	INT64 GetAdenaAmount();

	class CItem* GetFirstItemByClassID(int classId, bool b);
	class CItem* GetByDBID(int id);
	class CItem* GetByServerID(int id);
	class CCreature* GetOwner();
	bool CheckAddableMulti(std::pair<UINT32, INT64> *items, bool b);
	class CItem* GetFirstItemBy(ItemFunctor &functor);
	std::list<CItem*> GetTradableItems(class CUser *user);
	bool SetInventoryChanged(class CItem *item, InventoryChange change);
	bool Pop(class CItem *item);
	void Push(class CItem *item);
	bool HaveItemByClassId(int classId, INT64 count, bool b);
	bool CheckAddable(int classId, INT64 count, bool b);

	/* 0x0000 */ unsigned char padding0x0000[0x0008 - 0x0000];
	/* 0x0008 */ class CSharedRWLock *lock;
	/* 0x0010 */ unsigned char padding0x0010[0x00C8 - 0x0010];
	/* 0x00C8 */ unsigned char noDropItems;
	/* 0x00C9 */ unsigned char padding0x00C9[0x00D0 - 0x00C9];
	/* 0x00D0 */
};

