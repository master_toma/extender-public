
#pragma once

#include <Windows.h>

class CObjectDB {
private:
	CObjectDB();
	CObjectDB(const CObjectDB &other);
	~CObjectDB();

public:
	static CObjectDB* Instance();

	int GetClassIdFromName(const wchar_t *name) const;
	class CItem* CreateItem(const int itemType);
	class CObject *GetObject(const int id);
	class CItem* GetItem(const int id);

	static bool IsNectarGourd(const int classId);
	static bool IsChronoGourd(const int classId);
};

