
#pragma once

#include <Windows.h>

class Crest {
public:
	static bool Crest::Check(const unsigned char *data, const size_t size);

	static const unsigned char clanCrest[256];
	static const unsigned char allyCrest[192];
	static const unsigned char clanEmblem[2176];
};

