
#include <Server/CNPC.h>
#include <Server/EventDrop.h>
#include <Server/CSkillAction2.h>
#include <Common/Utils.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CLog.h>
#include <vector>

void CNPC::Init()
{
	WriteInstructionCall(0x7B7168 + 0x5C7, reinterpret_cast<UINT32>(DieWrapper));
	WriteInstructionCall(0x880B1C + 0x8B, reinterpret_cast<UINT32>(DieWrapper));
	WriteMemoryQWORD(0xA73848, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xAB9758, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xB1FB08, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xB7D3A8, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xB93FD8, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xC32068, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xC393D8, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xC3BDF8, reinterpret_cast<UINT64>(DieWrapper));
	WriteMemoryQWORD(0xC8C9D8, reinterpret_cast<UINT64>(DieWrapper));
	WriteInstructionCall(0x7386C6, FnPtr(&CNPC::OnNpcUseSkillPacket));

	WriteMemoryQWORD(0xA73368, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xAB9278, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xB1F628, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xB7CEC8, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xB93AF8, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xC31B88, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xC38EF8, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xC39A78, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xC3B918, FnPtr(&CNPC::IsMakeAttackerGuilty));
	WriteMemoryQWORD(0xC8C4F8, FnPtr(&CNPC::IsMakeAttackerGuilty));
}

bool __cdecl CNPC::DieWrapper(CNPC *self, CCreature *killer)
{
	return self->Die(killer);
}

bool CNPC::Die(CCreature *killer)
{
	GUARDED;

	if (this && killer && !IsUser() && !IsSummon() && (killer->IsUser() || killer->IsSummon())) {
		EventDrop::Data *data = EventDrop::data;
		if (data->maxLevelDifference < 0 || sd->level > killer->sd->level || killer->sd->level - sd->level <= data->maxLevelDifference) {
			double r = RandomDouble() * 100.0;
			double s = 0.0;
			for (size_t i = 0 ; i < data->items.size() ; ++i) {
				s += data->items[i].chance;
				if (s > r) {
					AddItemToInventory(data->items[i].itemId, 1);
					break;
				}
			}
		}
	}

	return reinterpret_cast<bool(*)(CNPC*, CCreature*)>(0x72972C)(this, killer);
}

bool CNPC::OnNpcUseSkillPacket(int targetId, int skillId, int npcAttackMoveType, int unknown)
{
	CCreature *target = reinterpret_cast<CCreature*>(CObject::GetObject(targetId));
	if (target && target->IsValidCreature()) {
		if (target->hide) return false;
	}
	return reinterpret_cast<bool(*)(CNPC*, int, int, int, int)>(0x71D598)(this, targetId, skillId, npcAttackMoveType, unknown);
}

bool CNPC::IsMakeAttackerGuilty(CCreature *creature, bool b)
{
	bool ret = reinterpret_cast<bool(*)(CNPC*, CCreature*, bool)>(0x46986C)(this, creature, b);
	if (!ret || !creature  || !creature->IsUserOrSummon() || !b) {
		return ret;
	}
	if (!Guard::WasCalled(reinterpret_cast<const wchar_t*>(0xB92050))) {
		// not originating from CSkillAction2::Activate
		return ret;
	}
	CSkillAction2 *skillAction = CSkillAction2::lastSkillActions[GetThreadIndex()];
	std::vector<void*> &effects = skillAction->skillInfo->effects[0];
	if (effects.size() != 1) {
		return ret;
	}
	if (*reinterpret_cast<UINT64*>(effects.back()) == 0xCF8808) {
		// CSkillEffect_i_transfer_hate
		return false;
	}
	return ret;
}

