
#pragma once

#include <Server/CPartyRoomInfo.h>

class CPartyRoom {
public:
	CPartyRoomInfo* GetInfo();

	/* 0x0000 */ unsigned char padding0x0000[0x0018-0x0000];
	/* 0x0018 */ CPartyRoomInfo roomInfo;
};

