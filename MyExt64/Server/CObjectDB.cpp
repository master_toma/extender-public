
#include <Server/CObjectDB.h>
#include <Server/CItem.h>

CObjectDB* CObjectDB::Instance()
{
	return reinterpret_cast<CObjectDB*>(0xE3DB6D0);
}

int CObjectDB::GetClassIdFromName(const wchar_t *name) const
{
	return reinterpret_cast<int(*)(const CObjectDB*, const wchar_t*)>(0x76199C)(this, name);
}

CItem* CObjectDB::CreateItem(const int itemType)
{
	return reinterpret_cast<CItem*(*)(CObjectDB*, const int)>(0x762054)(this, itemType);
}

CObject* CObjectDB::GetObject(const int id)
{
	return reinterpret_cast<CObject*(*)(CObjectDB*, const int)>(0x761AF4)(this, id);
}

CItem* CObjectDB::GetItem(const int id)
{
	CObject *object = GetObject(id);
	if (!object->IsItem()) return 0;
	return reinterpret_cast<CItem*>(object);
}

bool CObjectDB::IsNectarGourd(const int classId)
{
	switch (classId) {
	case 1012774: // small_baby_gourd
	case 1012777: // big_baby_gourd
	case 1013271: // ev_unripe_watermelon
	case 1013275: // ev_unripe_h_watermelon
		return true;
	default:
		return false;
	}
}

bool CObjectDB::IsChronoGourd(const int classId)
{
	switch (classId) {
	case 1012778: // g_big_adult_gourd
	case 1012779: // b_big_adult_gourd
	case 1013017: // kg_big_adult_gourd
	case 1013276: // ev_bad_h_watermelon
	case 1013277: // ev_great_h_watermelon
	case 1013278: // ev_kgreat_h_watermelon
		return true;
	default:
		return false;
	}
}

