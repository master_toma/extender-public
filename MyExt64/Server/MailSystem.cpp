
#include <Server/MailSystem.h>
#include <Server/CUserSocket.h>
#include <Server/CUser.h>
#include <Server/CInventory.h>
#include <Server/CItem.h>
#include <Server/CDB.h>
#include <Server/CObjectDB.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CSharedRWLock.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Common/Config.h>
#include <time.h>

namespace {

void LogPacket(const unsigned char *packet, const wchar_t *name)
{
	CLog::Add(CLog::Blue, L"%s: Packet %02X:%04X:",
		name, packet[-3], *reinterpret_cast<const UINT16*>(&packet[-2]));
	size_t size = *reinterpret_cast<const UINT16*>(&packet[-5]) - 5;
	for (size_t i = 0 ; i < size ; ) {
		std::wstring s;
		for (size_t j = 0 ; j < 16 && i + j < size ; ++j) {
			wchar_t buf[4];
			wsprintf(buf, L"%02X ", packet[i + j]);
			s.append(buf);
		}
		CLog::Add(CLog::Blue, L"%04X: %s", i, s.c_str());
		i += 16;
	}
}

CUser* CheckUser(CUserSocket *socket,
                 const int peaceZoneMessage = 2976,
                 const int tradeMessage = 2977,
                 const int storeMessage = 2978,
                 const int enchantingMessage = 2979)
{
	if (!Config::Instance()->mail->enabled) return 0;
	CUser *user = socket->user;
	if (!user || !user->sd) return 0;
	if (!user->sd->isInsidePeaceZone) {
		socket->SendSystemMessage(peaceZoneMessage);
		return 0;
	}
	if (user->IsNowTrade()) {
		socket->SendSystemMessage(tradeMessage);
		return 0;
	}
	if (storeMessage && user->sd->storeMode) {
		socket->SendSystemMessage(storeMessage);
		return 0;
	}
	if (enchantingMessage && user->ext.guard.isEnchanting) {
		socket->SendSystemMessage(enchantingMessage);
		return 0;
	}
	return user;
}

} // namespace

bool MailSystem::RequestPostItemListPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket, 2970, 2971, 2972, 2973);
	if (!user) return false;

	CSharedRWLockReadGuard guard(user->inventory.lock);

	char buffer[0x2000];
	size_t size = 0;
	size_t count = 0;

	std::list<CItem*> items = user->inventory.GetTradableItems(user);
	for (std::list<CItem*>::const_iterator i = items.begin() ; i != items.end() ; ++i) {
		CItem *item = *i;
		size_t itemSize = Assemble(buffer + size, sizeof(buffer) - size, "ddQhhdhhhhhhhhhhhhh",
			item->sd->objectId,
			item->sd->classId,
			item->sd->count,
			item->sd->itemType2,
			0,
			item->sd->bodyPart,
			item->sd->enchant,
			0,
			item->attributeAttackType,
			item->attributeAttackValue,
			item->attributeFire,
			item->attributeWater,
			item->attributeWind,
			item->attributeEarth,
			item->attributeDivine,
			item->attributeDark,
			0,
			0,
			0);
		if (itemSize <= 0) {
			CLog::Add(CLog::Red, L"[%s][%d] Overflow in assemble post item list", __FILEW__, __LINE__);
			break;
		}
		size += itemSize;
		++count;
	}

	guard.Unlock();

	socket->Send("chdb", 0xFE, 0x00B2, count, size, buffer);

	return false;
}

bool MailSystem::RequestSendPostPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket, 2970, 2971, 2972, 2973);
	if (!user) return false;

	if (user->GetPendingUseETCItem(0) || user->GetPendingUseETCItem(2)) {
		CUserSocket *socket = user->socket;
		if (socket) socket->SendSystemMessage(2973);
		return false;
	}

	if (!user->IsItemUsable()) return false;

	wchar_t recipient[25];
	int cod = 0;
	wchar_t subject[128];
	wchar_t content[512];
	int attachmentCount = 0;

	packet = Disassemble(packet, "SdSSd",
		sizeof(recipient), recipient,
		&cod,
		sizeof(subject), subject,
		sizeof(content), content,
		&attachmentCount);

	if (attachmentCount < 0 || attachmentCount > 8) {
		CLog::Add(CLog::Red, L"User [%s] trying to send post with wrong attachment count %d",
			user->GetName(), attachmentCount);
		return false;
	}

	if (cod < 0 || cod > 0x1000000000000000L) {
		CLog::Add(CLog::Red, L"User [%s] trying to send post with wrong cod adena %ld",
			user->GetName(), cod);
		return false;
	}

	INT64 price = 100 + attachmentCount * 1000;
	INT64 adena = user->inventory.GetAdenaAmount();

	char buffer[0x2000];
	size_t size = 0;

	std::set<int> itemObjectIds;
	for (size_t i = 0 ; i < attachmentCount ; ++i) {
		int objectId = 0;
		INT64 amount = 0;
		packet = Disassemble(packet, "dQ", &objectId, &amount);
		if (!itemObjectIds.insert(objectId).second) {
			CLog::Add(CLog::Red, L"User [%s] trying to send duplicate item", user->GetName());
			return true;
		}
		if (amount <= 0 || amount >= 0x1000000000000000L) {
			CLog::Add(CLog::Red, L"User [%s] trying to send post with attachment with count %lld",
				user->GetName(), amount);
			return false;
		}
		CItem *item = user->inventory.GetByServerID(objectId);
		if (!item) {
			CLog::Add(CLog::Red, L"User [%s] trying to send post with item not in inventory %d",
				user->GetName(), objectId);
			return false;
		}
		if (!item->IsTradeable(user)) {
			CLog::Add(CLog::Red, L"User [%s] trying to send post with non-tradable item %d",
				user->GetName(), objectId);
			return false;
		}
		if (user->IsEquippedItem(item)) {
			CLog::Add(CLog::Red, L"User [%s] trying to send post with equipped item %d", user->GetName(), objectId);
			return false;
		}
		if (item->sd->classId == 57) {
			adena -= amount;
		}
		size += Assemble(buffer + size, sizeof(buffer) - size, "ddQ", item->GetDBID(), item->sd->objectId, amount);
	}

	if (price > adena) {
		socket->SendSystemMessage(2975);
		return false;
	}

	INT64 codAdena = 0;
	if (cod) {
		Disassemble(packet, "Q", &codAdena);
	}

	CDB::Instance()->RequestSendPost(
		user, recipient, subject, content, attachmentCount, size, buffer, codAdena);

	return false;
}

void MailSystem::ReplySendPostRecipientNotFound(CUser *user)
{
	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}
	socket->SendSystemMessage(3002);
}

void MailSystem::ReplySendPostRecipientIsSender(CUser *user)
{
	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}
	socket->SendSystemMessage(3019);
}

void MailSystem::ReplySendPostRecipientBlockedSender(CUser *user)
{
	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}
	socket->SendSystemMessage(3082);
}

void MailSystem::ReplySendPostRecipientMailboxFull(CUser *user)
{
	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}
	socket->SendSystemMessage(3077);
}

void MailSystem::ReplySendPostNotEnoughAdena(CUser *user)
{
	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}
	socket->SendSystemMessage(2975);
}

void MailSystem::ReplySendPostDone(CUser *sender, CUser *recipient, const unsigned char *data)
{
	if (sender) {
		sender->TradeCancel();

		int itemCount = 0;
		data = Disassemble(data, "d", &itemCount);

		{
			CSharedRWLockWriteGuard lock(sender->inventory.lock);
			for (size_t i = 0 ; i < itemCount ; ++i) {
				int itemDbId = 0;
				INT64 amount = 0;
				data = Disassemble(data, "dQ", &itemDbId, &amount);
				CItem *item = sender->inventory.GetByDBID(itemDbId);
				if (!item) continue;
				if (amount) {
					{
						CYieldLockGuard lock(item->lock);
						item->sd->count = amount;
					}
					sender->inventory.SetInventoryChanged(item, CInventory::UPDATE);
				} else {
					if (sender->IsEquippedItem(item)) {
						sender->UnequipItem(item);
					}
					sender->inventory.Pop(item);
					sender->inventory.SetInventoryChanged(item, CInventory::REMOVE);
					item->Delete();
				}
			}
		}

		sender->SendItemList(true, false, 0);

		CUserSocket *senderSocket = sender->socket;
		if (!senderSocket) {
			CLog::Add(CLog::Red, L"User [%s]: no socket", sender->GetName());
		} else {
			senderSocket->SendSystemMessage(3009);
			senderSocket->Send("chd", 0xFE, 0x00B4, 1);
		}
	}

	if (recipient) {
		CUserSocket *recipientSocket = recipient->socket;
		if (!recipientSocket) {
			CLog::Add(CLog::Red, L"User [%s]: no socket", recipient->GetName());
		} else {
			recipientSocket->SendSystemMessage(3008);
			recipientSocket->Send("chd", 0xFE, 0x00A9, 1);
		}
	}
}

bool MailSystem::RequestReceivePostListPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket);
	if (!user) return false;

	CDB::Instance()->RequestReceivePostList(user);
	return false;
}

void MailSystem::ReplyReceivePostList(class CUser *user, const unsigned char *data)
{
	GUARDED;

	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}

	time_t now = time(0);

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t postsWritten = 0;

	int postCount = 0;
	data = Disassemble(data, "d", &postCount);
	for (size_t i = 0 ; i < postCount ; ++i) {
		int id = 0;
		UINT64 createDate = 0;
		UINT64 expireDate = 0;
		wchar_t sender[25];
		wchar_t subject[128];
		int read = 0;
		int isCod = 0;
		int attachments = 0;
		data = Disassemble(data, "dQQSSddd",
			&id, &createDate, &expireDate,
			sizeof(sender), sender,
			sizeof(subject), subject,
			&read, &isCod, &attachments);
		if (expireDate > now + 60 * 60 * 24 * 999) expireDate = now + 60 * 60 * 24 * 999;
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten,
			"dSSddddddd", id, subject, sender, 0, UINT32(expireDate), read == 0, 1, attachments, 0, 0);
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble received post list");
			break;
		}
		bytesWritten += n;
		++postsWritten;
	}
	socket->Send("chddb", 0xFE, 0x00AA, now, postsWritten, bytesWritten, buffer);
}

bool MailSystem::RequestDeleteReceivedPostPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket);
	if (!user) return false;

	int count = 0;
	packet = Disassemble(packet, "d", &count);
	std::vector<int> ids;
	for (size_t i = 0 ; i < count ; ++i) {
		int id = 0;
		packet = Disassemble(packet, "d", &id);
		ids.push_back(id);
	}
	CDB::Instance()->RequestDeleteReceivedPost(user, ids);
	return false;
}

void MailSystem::ReplyDeleteReceivedPost(class CUser *user, const unsigned char *data)
{
	GUARDED;

	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}

	int count = 0;
	data = Disassemble(data, "d", &count);
	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t idsWritten = 0;
	for (size_t i = 0 ; i < count ; ++i) {
		int id = 0;
		data = Disassemble(data, "d", &id);
		{
			ScopedLock lock(user->ext.mail.mailCS);
			std::map<int, CUser::Ext::Mail::Message>::iterator j = user->ext.mail.receivedMessages.find(id);
			if (j != user->ext.mail.receivedMessages.end()) user->ext.mail.receivedMessages.erase(j);
		}
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "dd", id, 0);
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble deleted post ids");
			break;
		}
		bytesWritten += n;
		++idsWritten;
	}
	socket->Send("chddb", 0xFE, 0x00B3, 1, idsWritten, bytesWritten, buffer);
}

bool MailSystem::RequestReceivedPostPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket);
	if (!user) return false;

	int id = 0;
	Disassemble(packet, "d", &id);
	CDB::Instance()->RequestReceivedPost(user, id);
	return false;
}

void MailSystem::ReplyReceivedPost(class CUser *user, const unsigned char *data)
{
	GUARDED;

	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t attachmentsWritten = 0;

	int id = 0;
	wchar_t sender[25];
	wchar_t subject[128];
	wchar_t content[512];
	int attachments = 0;
	INT64 adena = 0;
	int systemMail = 0;
	data = Disassemble(data, "dSSSQdd",
		&id,
		sizeof(sender), sender,
		sizeof(subject), subject,
		sizeof(content), content,
		&adena,
		&systemMail,
		&attachments);
	CUser::Ext::Mail::Message message(id, adena);
	for (size_t i = 0 ; i < attachments ; ++i) {
		int itemType = 0;
		int itemId = 0;
		INT64 amount = 0;
		int enchant = 0;
		INT16 attribute[9];
		data = Disassemble(data, "ddQdhhhhhhhh", &itemType, &itemId, &amount, &enchant,
			&attribute[0], &attribute[1], &attribute[2], &attribute[3],
			&attribute[4], &attribute[5], &attribute[6], &attribute[7]);
		attribute[8] = 0;
		CItem *item = CObjectDB::Instance()->GetItem(itemType);
		if (!item) {
			CLog::Add(CLog::Red, L"Item type %d doesn't exist", itemType);
			continue;
		}
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "hddQddddhhhhhhhhhhhhh",
			item->sd->itemType2, itemId, itemType, amount, item->sd->bodyPart, enchant,
			0, 0, 0, 0,
			attribute[0], attribute[1], attribute[2], attribute[3],
			attribute[4], attribute[5], attribute[6], attribute[7],
			0, 0, 0);
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble received post items");
			break;
		}
		message.attachments.push_back(CUser::Ext::Mail::Message::Attachment(itemType, amount));
		bytesWritten += n;
		++attachmentsWritten;
	}
	{
		ScopedLock lock(user->ext.mail.mailCS);
		std::pair<std::map<int, CUser::Ext::Mail::Message>::iterator, bool> result = user->ext.mail.receivedMessages.insert(std::make_pair(message.id, message));
		if (!result.second) result.first->second = message;
	}
	socket->Send("chdddSSSdbQdd", 0xFE, 0x00AB, id, adena ? 1 : 0, 0, sender, subject, content,
		attachmentsWritten, bytesWritten, buffer, adena, systemMail ? 0 : 1, 0);
	socket->Send("chdddd", 0xFE, 0x00B3, 1, 1, id, 1);
}

bool MailSystem::RequestReceivePostPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket, 2976, 2977, 2978, 2979);
	if (!user) return false;

	if (user->GetPendingUseETCItem(0) || user->GetPendingUseETCItem(2)) {
		CUserSocket *socket = user->socket;
		if (socket) socket->SendSystemMessage(2979);
		return false;
	}

	if (!user->IsItemUsable()) return false;

	int id = 0;
	Disassemble(packet, "d", &id);

	{
		ScopedLock lock(user->ext.mail.mailCS);
		std::map<int, CUser::Ext::Mail::Message>::const_iterator i = user->ext.mail.receivedMessages.find(id);
		if (i == user->ext.mail.receivedMessages.end()) {
			return false;
		}
		std::pair<UINT32, INT64> items[200];
		memset(items, 0, sizeof(items));
		size_t index = 0;
		for (std::vector<CUser::Ext::Mail::Message::Attachment>::const_iterator j = i->second.attachments.begin() ;
			j != i->second.attachments.end() ; ++j, ++index) {

			items[index].first = j->itemType;
			items[index].second = j->amount;
		}
		CSharedRWLockWriteGuard lock2(user->inventory.lock);
		if (user->inventory.GetAdenaAmount() < i->second.codAdena) {
			socket->SendSystemMessage(2980);
			return false;
		}
		if (!user->inventory.CheckAddableMulti(items, false)) {
			socket->SendSystemMessage(2981);
			return false;
		}
	}

	CDB::Instance()->RequestReceivePost(user, id);
	return false;
}

void MailSystem::ReplyReceivePost(class CUser *user, const unsigned char *data)
{
	if (!user) return;

	user->TradeCancel();

	int id = 0;
	int itemCount = 0;
	int senderDbId = 0;
	int adenaItemDbId = 0;
	INT64 adena = 0;
	INT64 codAdena = 0;
	data = Disassemble(data, "dddQQd", &id, &senderDbId, &adenaItemDbId, &adena, &codAdena, &itemCount);

	{
		CSharedRWLockWriteGuard lock(user->inventory.lock);
		for (size_t i = 0 ; i < itemCount ; ++i) {
			int itemType = 0;
			int itemDbId = 0;
			INT64 amount = 0;
			int enchant = 0;
			INT16 attribute[9];
			data = Disassemble(data, "ddQdhhhhhhhh", &itemType, &itemDbId, &amount, &enchant,
				&attribute[0], &attribute[1], &attribute[2], &attribute[3],
				&attribute[4], &attribute[5], &attribute[6], &attribute[7]);
			attribute[8] = 0;
			CItem *item = user->inventory.GetByDBID(itemDbId);
			if (item) {
				if (amount) {
					{
						CYieldLockGuard lock(item->lock);
						item->sd->count = amount;
					}
					user->inventory.SetInventoryChanged(item, CInventory::UPDATE);
				} else {
					user->inventory.Pop(item);
					user->inventory.SetInventoryChanged(item, CInventory::REMOVE);
					item->Delete();
				}
			} else {
				item = CObjectDB::Instance()->CreateItem(itemType);
				if (!item) {
					CLog::Add(CLog::Red, L"Can't create item of type %d", itemType);
				} else {
					item->sd->count = amount;
					item->sd->enchant = enchant;
					item->SetDBID(itemDbId);
					item->SetAttribute(attribute);
					user->inventory.Push(item);
					user->inventory.SetInventoryChanged(item, CInventory::CREATE);
				}
			}
		}
	}

	SmartPtr<CUser> senderSP;
	if (senderDbId) {
		senderSP = SmartPtr<CUser>::FromDBID(senderDbId);
	}
	if (senderSP) {
		if (adenaItemDbId) {
			senderSP->TradeCancel();
			CSharedRWLockWriteGuard lock(senderSP->inventory.lock);
			CItem *item = senderSP->inventory.GetByDBID(adenaItemDbId);
			if (item) {
				{
					CYieldLockGuard lock(item->lock);
					item->sd->count = adena;
				}
				senderSP->inventory.SetInventoryChanged(item, CInventory::UPDATE);
			} else {
				item = CObjectDB::Instance()->CreateItem(57);
				if (!item) {
					CLog::Add(CLog::Red, L"Can't create item of type 57");
				} else {
					INT16 attribute[9];
					memset(attribute, 0, sizeof(attribute));
					attribute[0] = -2;
					item->sd->count = adena;
					item->sd->enchant = 0;
					item->SetDBID(adenaItemDbId);
					item->SetAttribute(attribute);
					senderSP->inventory.Push(item);
					senderSP->inventory.SetInventoryChanged(item, CInventory::CREATE);
				}
			}
		}

		senderSP->SendItemList(true, false, 0);
		CUserSocket *socket = senderSP->socket;
		if (socket) {
			if (adenaItemDbId) {
				socket->Send("cdddQdS", 0x62, 3025, 2, 6, codAdena, 0, user->GetName());
			} else {
				socket->Send("cdddS", 0x62, 3072, 1, 0, user->GetName());
			}
		}
	}

	user->SendItemList(true, false, 0);
	CUserSocket *socket = user->socket;
	if (socket) socket->SendSystemMessage(3012);
	CDB::Instance()->RequestReceivePostList(user);
}

bool MailSystem::RequestRejectPostPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket, 2982, 2983, 2984, 2985);
	if (!user) return false;

	int id = 0;
	Disassemble(packet, "d", &id);

	CDB::Instance()->RequestRejectPost(user, id);
	return false;
}

void MailSystem::ReplyRejectPost(class CUser *user, const unsigned char *data)
{
	GUARDED;

	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}

	int id = 0;
	int senderDbId = 0;
	Disassemble(data, "dd", &id, &senderDbId);
	{
		ScopedLock lock(user->ext.mail.mailCS);
		std::map<int, CUser::Ext::Mail::Message>::iterator j = user->ext.mail.receivedMessages.find(id);
		if (j != user->ext.mail.receivedMessages.end()) user->ext.mail.receivedMessages.erase(j);
	}
	SmartPtr<CUser> senderSP = SmartPtr<CUser>::FromDBID(senderDbId);
	if (senderSP) {
		CUserSocket *senderSocket = senderSP->socket;
		if (senderSocket) {
			senderSocket->SendSystemMessage(3008);
			senderSocket->Send("chd", 0xFE, 0x00A9, 1);
		}
	}
	socket->SendSystemMessage(3010);
	CDB::Instance()->RequestReceivePostList(user);
}

bool MailSystem::RequestSentPostListPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket);
	if (!user) return false;

	CDB::Instance()->RequestSentPostList(user);
	return false;
}

void MailSystem::ReplySentPostList(class CUser *user, const unsigned char *data)
{
	GUARDED;

	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}

	time_t now = time(0);

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t postsWritten = 0;

	int postCount = 0;
	data = Disassemble(data, "d", &postCount);
	for (size_t i = 0 ; i < postCount ; ++i) {
		int id = 0;
		INT64 createDate = 0;
		INT64 expireDate = 0;
		wchar_t recipient[25];
		wchar_t subject[128];
		int read = 0;
		int isCod = 0;
		int attachments = 0;
		data = Disassemble(data, "dQQSSddd",
			&id, &createDate, &expireDate,
			sizeof(recipient), recipient,
			sizeof(subject), subject,
			&read, &isCod, &attachments);
		if (expireDate > now + 60 * 60 * 24 * 999) expireDate = now + 60 * 60 * 24 * 999;
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten,
			"dSSddddd", id, subject, recipient, 0, int(expireDate), read == 0, 1, attachments);
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble sent post list");
			break;
		}
		bytesWritten += n;
		++postsWritten;
	}
	socket->Send("chddb", 0xFE, 0x00AC, now, postsWritten, bytesWritten, buffer);
}

bool MailSystem::RequestDeleteSentPostListPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket);
	if (!user) return false;

	int count = 0;
	packet = Disassemble(packet, "d", &count);
	std::vector<int> ids;
	for (size_t i = 0 ; i < count ; ++i) {
		int id = 0;
		packet = Disassemble(packet, "d", &id);
		ids.push_back(id);
	}
	CDB::Instance()->RequestDeleteSentPost(user, ids);
	return false;
}

void MailSystem::ReplyDeleteSentPost(class CUser *user, const unsigned char *data)
{
	GUARDED;

	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}

	int count = 0;
	data = Disassemble(data, "d", &count);
	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t idsWritten = 0;
	for (size_t i = 0 ; i < count ; ++i) {
		int id = 0;
		data = Disassemble(data, "d", &id);
		{
			ScopedLock lock(user->ext.mail.mailCS);
			std::map<int, CUser::Ext::Mail::Message>::iterator j = user->ext.mail.sentMessages.find(id);
			if (j != user->ext.mail.sentMessages.end()) user->ext.mail.sentMessages.erase(j);
		}
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "dd", id, 0);
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble deleted post ids");
			break;
		}
		bytesWritten += n;
		++idsWritten;
	}
	socket->Send("chddb", 0xFE, 0x00B3, 0, idsWritten, bytesWritten, buffer);
}

bool MailSystem::RequestSentPostPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket);
	if (!user) return false;

	int id = 0;
	Disassemble(packet, "d", &id);
	CDB::Instance()->RequestSentPost(user, id);
	return false;
}

void MailSystem::ReplySentPost(class CUser *user, const unsigned char *data)
{
	GUARDED;

	CUserSocket *socket = user->socket;
	if (!socket) {
		CLog::Add(CLog::Red, L"User [%s]: no socket", user->GetName());
		return;
	}

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t attachmentsWritten = 0;

	int id = 0;
	wchar_t recipient[25];
	wchar_t subject[128];
	wchar_t content[512];
	int attachments = 0;
	INT64 adena = 0;
	data = Disassemble(data, "dSSSQd",
		&id,
		sizeof(recipient), recipient,
		sizeof(subject), subject,
		sizeof(content), content,
		&adena,
		&attachments);
	CUser::Ext::Mail::Message message(id, adena);
	for (size_t i = 0 ; i < attachments ; ++i) {
		int itemType = 0;
		int itemId = 0;
		INT64 amount = 0;
		int enchant = 0;
		INT16 attribute[9];
		data = Disassemble(data, "ddQdhhhhhhhh", &itemType, &itemId, &amount, &enchant,
			&attribute[0], &attribute[1], &attribute[2], &attribute[3],
			&attribute[4], &attribute[5], &attribute[6], &attribute[7]);
		attribute[8] = 0;
		CItem *item = CObjectDB::Instance()->GetItem(itemType);
		if (!item) {
			CLog::Add(CLog::Red, L"Item type %d doesn't exist", itemType);
			continue;
		}
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "hddQddddhhhhhhhhhhhhh",
			item->sd->itemType2, itemId, itemType, amount, item->sd->bodyPart, enchant,
			0, 0, 0, 0,
			attribute[0], attribute[1], attribute[2], attribute[3],
			attribute[4], attribute[5], attribute[6], attribute[7],
			0, 0, 0);
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble sent post items");
			break;
		}
		message.attachments.push_back(CUser::Ext::Mail::Message::Attachment(itemType, amount));
		bytesWritten += n;
		++attachmentsWritten;
	}
	{
		ScopedLock lock(user->ext.mail.mailCS);
		std::pair<std::map<int, CUser::Ext::Mail::Message>::iterator, bool> result = user->ext.mail.sentMessages.insert(std::make_pair(message.id, message));
		if (!result.second) result.first->second = message;

	}
	socket->Send("chddSSSdbQd", 0xFE, 0x00AD, id, adena ? 1 : 0, recipient, subject, content,
		attachmentsWritten, bytesWritten, buffer, adena, attachments ? 1 : 0);
}

bool MailSystem::RequestCancelSentPostPacket(class CUserSocket *socket, const unsigned char *packet)
{
	GUARDED;

	CUser *user = CheckUser(socket, 2982, 2983, 2984, 2985);
	if (!user) return false;

	if (user->GetPendingUseETCItem(0) || user->GetPendingUseETCItem(2)) {
		CUserSocket *socket = user->socket;
		if (socket) socket->SendSystemMessage(2985);
		return false;
	}

	if (!user->IsItemUsable()) return false;

	int id = 0;
	Disassemble(packet, "d", &id);

	{
		ScopedLock lock(user->ext.mail.mailCS);
		std::map<int, CUser::Ext::Mail::Message>::const_iterator i = user->ext.mail.sentMessages.find(id);
		if (i == user->ext.mail.sentMessages.end()) {
			return false;
		}
		std::pair<UINT32, INT64> items[200];
		memset(items, 0, sizeof(items));
		size_t index = 0;
		for (std::vector<CUser::Ext::Mail::Message::Attachment>::const_iterator j = i->second.attachments.begin() ;
			j != i->second.attachments.end() ; ++j, ++index) {

			items[index].first = j->itemType;
			items[index].second = j->amount;
		}
		CSharedRWLockWriteGuard lock2(user->inventory.lock);
		if (!user->inventory.CheckAddableMulti(items, false)) {
			socket->SendSystemMessage(2988);
			return false;
		}
	}

	CDB::Instance()->RequestCancelPost(user, id);
	return false;
}

void MailSystem::ReplyCancelSentPost(class CUser *user, const unsigned char *data)
{
	if (!user) return;

	int id = 0;
	int itemCount = 0;
	int recipientDbId = 0;
	data = Disassemble(data, "ddd", &id, &recipientDbId, &itemCount);

	if (!id) {
		CUserSocket *socket = user->socket;
		if (!socket) return;
		switch (recipientDbId) {
		case 0: socket->SendSystemMessage(3030); break;
		default: break;
		}
		return;
	}

	user->TradeCancel();

	{
		CSharedRWLockWriteGuard lock(user->inventory.lock);
		for (size_t i = 0 ; i < itemCount ; ++i) {
			int itemType = 0;
			int itemDbId = 0;
			INT64 amount = 0;
			int enchant = 0;
			INT16 attribute[9];
			data = Disassemble(data, "ddQdhhhhhhhh", &itemType, &itemDbId, &amount, &enchant,
				&attribute[0], &attribute[1], &attribute[2], &attribute[3],
				&attribute[4], &attribute[5], &attribute[6], &attribute[7]);
			attribute[8] = 0;
			CItem *item = user->inventory.GetByDBID(itemDbId);
			if (item) {
				if (amount) {
					{
						CYieldLockGuard lock(item->lock);
						item->sd->count = amount;
					}
					user->inventory.SetInventoryChanged(item, CInventory::UPDATE);
				} else {
					user->inventory.Pop(item);
					user->inventory.SetInventoryChanged(item, CInventory::REMOVE);
					item->Delete();
				}
			} else {
				item = CObjectDB::Instance()->CreateItem(itemType);
				if (!item) {
					CLog::Add(CLog::Red, L"Can't create item of type %d", itemType);
				} else {
					item->sd->count = amount;
					item->sd->enchant = enchant;
					item->SetDBID(itemDbId);
					item->SetAttribute(attribute);
					user->inventory.Push(item);
					user->inventory.SetInventoryChanged(item, CInventory::CREATE);
				}
			}
		}
	}

	SmartPtr<CUser> recipientSP;
	if (recipientDbId) {
		recipientSP = SmartPtr<CUser>::FromDBID(recipientDbId);
	}
	if (recipientSP) {
		CUserSocket *socket = recipientSP->socket;
		if (socket) socket->Send("cdddS", 0x62, 3067, 1, 0, user->GetName());
	}

	user->SendItemList(true, false, 0);
	CUserSocket *socket = user->socket;
	if (socket) socket->SendSystemMessage(3011);
	CDB::Instance()->RequestSentPostList(user);
}

void MailSystem::ReplyCheckMail(class CUser *user, const unsigned char *data)
{
	int count = 0;
	Disassemble(data, "d", &count);
	if (count) {
		CUserSocket *socket = user->socket;
		if (!socket) return;
		socket->Send("cdddd", 0x62, 1227, 1, 1, count);
		socket->Send("chd", 0xFE, 0x00A9, 1);
	}
}

void MailSystem::Upkeep()
{
	CDB::Instance()->RequestMailUpkeep();
}

void MailSystem::ReplyReturnedPost(const unsigned char *data)
{
	GUARDED;

	int senderDbId = 0;
	int recipientDbId = 0;
	int id = 0;
	Disassemble(data, "ddd", &senderDbId, &recipientDbId, &id);

	SmartPtr<CUser> senderSP = SmartPtr<CUser>::FromDBID(senderDbId);
	if (senderSP) {
		CUserSocket *socket = senderSP->socket;
		if (socket) {
			socket->SendSystemMessage(3068);
			socket->Send("chd", 0xFE, 0x00A9, 1);
		}
	}
	SmartPtr<CUser> recipientSP = SmartPtr<CUser>::FromDBID(recipientDbId);
	if (recipientSP) {
		CUserSocket *socket = recipientSP->socket;
		if (socket) socket->SendSystemMessage(3068);
	}
}

