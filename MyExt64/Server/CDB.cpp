
#include <Server/CDB.h>
#include <Server/CUser.h>
#include <Server/MailSystem.h>
#include <Server/CUserSocket.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Common/CSharedCreatureData.h>

void CDB::Init()
{
	WriteInstructionCall(0x443F58 + 0x11D, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x51EB88 + 0x11F, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x55CC2C + 0x2BF, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x562BFC + 0x1C1, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x576DD4 + 0x14C, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x577490 + 0x16C, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x76A03C + 0x112, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x771B94 + 0x13E, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x838AB8 + 0xFF, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x838C30 + 0xFF, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x838DA8 + 0xFF, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x839120 + 0x119, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x883F98 + 0x12B, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x88BE18 + 0xC2, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x899314 + 0x170, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8994DC + 0x161, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8A71A4 + 0x7C1, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8BE4C8 + 0x4B5, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8C09B4 + 0x2089, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8C09B4 + 0x23A9, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8C09B4 + 0x24E7, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8CB090 + 0x30B, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8D0640 + 0x19CF, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8EB694 + 0x26A, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x8F2664 + 0x85, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionCall(0x909858 + 0x219, reinterpret_cast<UINT32>(SendSaveCharacterInfoWrapper));
	WriteInstructionJmp(0x5B3598, FnPtr(ReplyExPacket));
}

CDB::CDB()
{
}

CDB::CDB(const CDB &other)
{
}

CDB::~CDB()
{
}

CDB* CDB::Instance()
{
	return reinterpret_cast<CDB*>(0x149AE80);
}

void CDB::RequestMarkInZoneRestriction(UINT32 userDbId, UINT32 zoneId, UINT32 time, UINT32 count, bool b, UINT32 i)
{
	reinterpret_cast<void(*)(CDB*, UINT32, UINT32, UINT32, UINT32, bool, UINT32)>(0x594D3C)(
		this, userDbId, zoneId, time, count, b, i);
}

void CDB::RequestLoadUserPoint(class CUser *user, int type)
{
	reinterpret_cast<void(*)(CDB*, class CUser*, int)>(0x59145C)(this, user, type);
}

void __cdecl CDB::SendSaveCharacterInfoWrapper(CDB *self, CUser *user, bool a, bool b)
{
	self->SendSaveCharacterInfo(user, a, b);
}

void CDB::SendSaveCharacterInfo(CUser *user, bool a, bool b)
{
	if (user && user->sd) {
		bool skip = false;
		{
			ScopedLock lock(user->ext.cs);
			if (!user->ext.famePointLoaded) {
				skip = true;
			}
		}
		if (skip) {
			CLog::Add(CLog::Red,
				L"User [%s]: can't save character: fame points not loaded yet (requesting load again)",
				user->sd->name);
			RequestLoadUserPoint(user, 5);
			return;
		}
	}
	reinterpret_cast<void(*)(CDB*, CUser*, bool, bool)>(0x5994C4)(this, user, a, b);
}

void CDB::SetDailyQuest(UINT32 charId, UINT32 questId, UINT32 completeTime)
{
	reinterpret_cast<void(*)(CDB*, UINT32, UINT32, UINT32)>(0x595798)(this, charId, questId, completeTime);
}

void CDB::RequestBuyItems(UINT32 userDbId, UINT32 npcId, CUser *user, INT64 price, UINT32 tax, void *pledge, UINT32 items, UINT32 dataSize, const char *data)
{
	reinterpret_cast<void(*)(CDB*, UINT32, UINT32, CUser*, INT64, UINT32, void*, UINT32, UINT32, const char*)>(0x581488)(
		this, userDbId, npcId, user, price, tax, pledge, items, dataSize, data);
}

bool CDB::ReplyExPacket(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	bool(*handler)(class CDBSocket*, const unsigned char*);
	if (opcode > 0x77) {
		handler = reinterpret_cast<bool(**)(class CDBSocket*, const unsigned char*)>(0x1633398)[0];
	} else if (opcode == 0x77) {
		handler = &ReplyExtPacket;
	} else {
		handler = reinterpret_cast<bool(**)(class CDBSocket*, const unsigned char*)>(0x16333A0)[opcode];
	}
	return handler(socket, packetData);

}

bool CDB::ReplyExtPacket(class CDBSocket *socket, const unsigned char *packet)
{
	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	switch (opcode) {
	case REPLY_TEST: return ReplyTest(socket, packetData);
	case REPLY_SEND_POST_RECIPIENT_NOT_FOUND: return ReplySendPostRecipientNotFound(socket, packetData);
	case REPLY_SEND_POST_RECIPIENT_IS_SENDER: return ReplySendPostRecipientIsSender(socket, packetData);
	case REPLY_SEND_POST_RECIPIENT_BLOCKED_SENDER: return ReplySendPostRecipientBlockedSender(socket, packetData);
	case REPLY_SEND_POST_RECIPIENT_MAILBOX_FULL: return ReplySendPostRecipientMailboxFull(socket, packetData);
	case REPLY_SEND_POST_NOT_ENOUGH_ADENA: return ReplySendPostNotEnoughAdena(socket, packetData);
	case REPLY_SEND_POST_DONE: return ReplySendPostDone(socket, packetData);
	case REPLY_RECEIVE_POST_LIST: return ReplyReceivePostList(socket, packetData);
	case REPLY_SENT_POST_LIST: return ReplySentPostList(socket, packetData);
	case REPLY_RECEIVED_POST: return ReplyReceivedPost(socket, packetData);
	case REPLY_SENT_POST: return ReplySentPost(socket, packetData);
	case REPLY_DELETE_RECEIVED_POST: return ReplyDeleteReceivedPost(socket, packetData);
	case REPLY_DELETE_SENT_POST: return ReplyDeleteSentPost(socket, packetData);
	case REPLY_RECEIVE_POST: return ReplyReceivePost(socket, packetData);
	case REPLY_CHECK_MAIL: return ReplyCheckMail(socket, packetData);
	case REPLY_REJECT_POST: return ReplyRejectPost(socket, packetData);
	case REPLY_CANCEL_POST: return ReplyCancelPost(socket, packetData);
	case REPLY_RETURNED_POST: return ReplyReturnedPost(socket, packetData);
	default:
		CLog::Add(CLog::Red, L"Unknown ExtPacket opcode %04X", opcode);
		return false;
	}
}

void CDB::RequestTest(int i)
{
	Send("chhd", 0xF8, 0x0033, REQUEST_TEST, i);
}

bool CDB::ReplyTest(class CDBSocket *socket, const unsigned char *packet)
{
	int i = 0;
	Disassemble(packet, "d", &i);
	CLog::Add(CLog::Blue, L"TestPacket reply :) %d", i);
	return false;
}

void CDB::RequestSendPost(CUser *user,
                          const wchar_t *recipient,
                          const wchar_t *subject,
                          const wchar_t *content,
                          int attachmentCount,
                          size_t attachmentsSize,
                          const char *attachments,
                          INT64 codAdena)
{
	GUARDED;

	Send("chhddSSSQdb", 0xF8, 0x0033, REQUEST_SEND_POST,
		user->GetDBID(), user->objectId,
		recipient, subject, content,
		codAdena, attachmentCount, attachmentsSize, attachments);
}

bool CDB::ReplySendPostRecipientNotFound(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplySendPostRecipientNotFound(&*userSP);
	return false;
}

bool CDB::ReplySendPostRecipientIsSender(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplySendPostRecipientIsSender(&*userSP);
	return false;
}

bool CDB::ReplySendPostRecipientBlockedSender(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplySendPostRecipientBlockedSender(&*userSP);
	return false;
}

bool CDB::ReplySendPostRecipientMailboxFull(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplySendPostRecipientMailboxFull(&*userSP);
	return false;
}

bool CDB::ReplySendPostNotEnoughAdena(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplySendPostNotEnoughAdena(&*userSP);
	return false;
}

bool CDB::ReplySendPostDone(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int senderObjectId = 0;
	int recipientDbId = 0;
	packet = Disassemble(packet, "dd", &senderObjectId, &recipientDbId);
	CUser *sender = 0;
	CUser *recipient = 0;
	SmartPtr<CUser> senderSP = SmartPtr<CUser>::FromObjectID(senderObjectId);
	if (senderSP) {
		sender = &*senderSP;
	} else {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", senderObjectId);
	}
	SmartPtr<CUser> recipientSP = SmartPtr<CUser>::FromDBID(recipientDbId);
	if (recipientSP) {
		recipient = &*recipientSP;
	}
	MailSystem::ReplySendPostDone(sender, recipient, packet);
	return false;
}

void CDB::RequestReceivePostList(CUser *user)
{
	Send("chhdd", 0xF8, 0x0033, REQUEST_RECEIVE_POST_LIST,
		user->GetDBID(), user->objectId);
}

bool CDB::ReplyReceivePostList(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	packet = Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplyReceivePostList(&*userSP, packet);
	return false;
}

void CDB::RequestSentPostList(CUser *user)
{
	Send("chhdd", 0xF8, 0x0033, REQUEST_SENT_POST_LIST,
		user->GetDBID(), user->objectId);
}

bool CDB::ReplySentPostList(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	packet = Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplySentPostList(&*userSP, packet);
	return false;
}

void CDB::RequestReceivedPost(CUser *user, const int id)
{
	Send("chhddd", 0xF8, 0x0033, REQUEST_RECEIVED_POST,
		user->GetDBID(), user->objectId, id);
}

bool CDB::ReplyReceivedPost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	packet = Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplyReceivedPost(&*userSP, packet);
	return false;
}

void CDB::RequestSentPost(CUser *user, const int id)
{
	Send("chhddd", 0xF8, 0x0033, REQUEST_SENT_POST,
		user->GetDBID(), user->objectId, id);
}

bool CDB::ReplySentPost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	packet = Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplySentPost(&*userSP, packet);
	return false;
}

void CDB::RequestDeleteReceivedPost(CUser *user, const std::vector<int> &ids)
{
	Send("chhdddb", 0xF8, 0x0033, REQUEST_DELETE_RECEIVED_POST,
		user->GetDBID(), user->objectId, ids.size(), ids.size() * sizeof(ids[0]), &ids[0]);
}

bool CDB::ReplyDeleteReceivedPost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	packet = Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplyDeleteReceivedPost(&*userSP, packet);
	return false;
}

void CDB::RequestDeleteSentPost(CUser *user, const std::vector<int> &ids)
{
	Send("chhdddb", 0xF8, 0x0033, REQUEST_DELETE_SENT_POST,
		user->GetDBID(), user->objectId, ids.size(), ids.size() * sizeof(ids[0]), &ids[0]);
}

bool CDB::ReplyDeleteSentPost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int objectId = 0;
	packet = Disassemble(packet, "d", &objectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(objectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", objectId);
		return false;
	}
	MailSystem::ReplyDeleteSentPost(&*userSP, packet);
	return false;
}

void CDB::RequestReceivePost(CUser *user, const int id)
{
	Send("chhddd", 0xF8, 0x0033, REQUEST_RECEIVE_POST,
		user->GetDBID(), user->objectId, id);
}

bool CDB::ReplyReceivePost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int userObjectId = 0;
	packet = Disassemble(packet, "d", &userObjectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(userObjectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", userObjectId);
		return false;
	}
	MailSystem::ReplyReceivePost(&*userSP, packet);
	return false;
}

void CDB::RequestCheckMail(CUser *user)
{
	Send("chhdd", 0xF8, 0x0033, REQUEST_CHECK_MAIL,
		user->GetDBID(), user->objectId);
}

bool CDB::ReplyCheckMail(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int userObjectId = 0;
	packet = Disassemble(packet, "d", &userObjectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(userObjectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", userObjectId);
		return false;
	}
	MailSystem::ReplyCheckMail(&*userSP, packet);
	return false;
}

void CDB::RequestRejectPost(CUser *user, const int id)
{
	Send("chhddd", 0xF8, 0x0033, REQUEST_REJECT_POST,
		user->GetDBID(), user->objectId, id);
}

bool CDB::ReplyRejectPost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int userObjectId = 0;
	packet = Disassemble(packet, "d", &userObjectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(userObjectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", userObjectId);
		return false;
	}
	MailSystem::ReplyRejectPost(&*userSP, packet);
	return false;
}

void CDB::RequestCancelPost(CUser *user, const int id)
{
	Send("chhddd", 0xF8, 0x0033, REQUEST_CANCEL_POST,
		user->GetDBID(), user->objectId, id);
}

bool CDB::ReplyCancelPost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int userObjectId = 0;
	packet = Disassemble(packet, "d", &userObjectId);
	SmartPtr<CUser> userSP = SmartPtr<CUser>::FromObjectID(userObjectId);
	if (!userSP) {
		CLog::Add(CLog::Red, L"Can't find user by objectId = %d", userObjectId);
		return false;
	}
	MailSystem::ReplyCancelSentPost(&*userSP, packet);
	return false;
}

void CDB::RequestMailUpkeep()
{
	Send("chh", 0xF8, 0x0033, REQUEST_MAIL_UPKEEP);
}

bool CDB::ReplyReturnedPost(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;
	MailSystem::ReplyReturnedPost(packet);
	return false;
}

