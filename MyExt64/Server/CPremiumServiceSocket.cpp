
#include <Server/CPremiumServiceSocket.h>
#include <stdarg.h>

void CPremiumServiceSocket::Send(const char* format, ...)
{
	va_list va;
	va_start(va, format);
	reinterpret_cast<void(*)(CPremiumServiceSocket*, const char*, va_list)>(0x7EB1A8)(this, format, va);
	va_end(va);
}

